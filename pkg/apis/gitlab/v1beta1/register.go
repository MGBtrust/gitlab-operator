/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// NOTE: Boilerplate only.  Ignore this file.

// Package v1beta1 contains API Schema definitions for the gitlab v1beta1 API group
// +k8s:openapi-gen=true
// +k8s:deepcopy-gen=package,register
// +k8s:conversion-gen=gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis/gitlab
// +k8s:defaulter-gen=TypeMeta
// +groupName=gitlab.com
package v1beta1

import (
	"fmt"
	"os"

	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/runtime/scheme"
)

func getGroupName() string {
	baseGroupName := "gitlab.com"
	crdPrefix := os.Getenv("CRD_PREFIX")
	if crdPrefix == "" {
		return baseGroupName
	}

	return fmt.Sprintf("%s.%s", crdPrefix, baseGroupName)
}

var (
	// SchemeGroupVersion is group version used to register these objects
	SchemeGroupVersion = schema.GroupVersion{Group: getGroupName(), Version: "v1beta1"}

	// SchemeBuilder is used to add go types to the GroupVersionKind scheme
	SchemeBuilder = &scheme.Builder{GroupVersion: SchemeGroupVersion}
)
