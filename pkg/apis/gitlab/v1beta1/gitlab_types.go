/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// GitLabSpec defines the desired state of GitLab
type GitLabSpec struct {
	Version     string    `json:"version"`
	Revision    string    `json:"revision,omitempty"`
	Templates   Templates `json:"templates"`
	HelmRelease string    `json:"helmRelease"`
}

type Templates struct {
	MigrationsTemplate    TemplateSpec      `json:"migrationsTemplate"`
	SharedSecretsTemplate SharedSecretsSpec `json:"sharedSecretsTemplate"`
}

type TemplateSpec struct {
	ConfigMapName string `json:"configMapName"`
	ConfigMapKey  string `json:"configMapKey"`
}

type SharedSecretsSpec struct {
	ServiceAccountKey string `json:"serviceAccountKey"`
	RoleKey           string `json:"roleKey"`
	RoleBindingKey    string `json:"roleBindingKey"`
	TemplateSpec      `json:",inline"`
}

const (
	// Failure is added when the controller encounters an error during reconcile
	ConditionFailure ConditionType = "Failure"
)

type Condition struct {
	Type   ConditionType          `json:"type"`
	Status corev1.ConditionStatus `json:"status"`

	Reason  string `json:"reason,omitempty"`
	Message string `json:"message,omitempty"`

	LastTransitionTime metav1.Time `json:"lastTransitionTime"`
}

// ConditionType defines conditions of a GitLab resource.
type ConditionType string

// GitLabStatus defines the observed state of GitLab
type GitLabStatus struct {
	ObservedGeneration int64  `json:"observedGeneration,omitempty"`
	DeployedRevision   string `json:"deployedRevision,omitempty"`

	Conditions []Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// GitLab is the Schema for the gitlabs API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type GitLab struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GitLabSpec   `json:"spec,omitempty"`
	Status GitLabStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// GitLabList contains a list of GitLab resources
type GitLabList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitLab `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GitLab{}, &GitLabList{})
}
