#!/bin/bash

function setup_gopath() {
  BUILD_DIR=$(pwd)
  export GOPATH=${BUILD_DIR}/_build
  export PATH=${GOPATH}/bin:${PATH}
  curl -L https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 >/tmp/dep && chmod +x /tmp/dep && mv /tmp/dep /usr/local/bin/
  mkdir -p "${GOPATH}/src/${PKG_PATH}"
  mount --bind "${CI_PROJECT_DIR}" "${GOPATH}/src/${PKG_PATH}"
}

function install_kubebuilder() {
  curl -L https://github.com/kubernetes-sigs/kubebuilder/releases/download/v1.0.3/kubebuilder_1.0.3_linux_amd64.tar.gz >/tmp/kubebuilder.tar.gz
  tar -zxvf /tmp/kubebuilder.tar.gz
  mv kubebuilder_1.0.3_linux_amd64 /usr/local/kubebuilder
  export PATH=$PATH:/usr/local/kubebuilder/bin
}

function install_docker() {
  wget -q https://get.docker.com/builds/Linux/x86_64/docker-1.13.1.tgz -O /tmp/docker.tar.gz; \
  tar -xzf /tmp/docker.tar.gz -C /tmp/
  cp /tmp/docker/docker* /usr/local/bin
  chmod +x /usr/local/bin/docker*
  rm -rf /tmp/docker
}

function build_image() {
  make dep generate fmt vet manifests
  docker build . --cache-from "$CI_REGISTRY_IMAGE:${CI_COMMIT_SHA}" -t "$CI_REGISTRY_IMAGE:${CI_COMMIT_SHA}"
}

function push_tags() {
  docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_SHA}"

  for tag in "$@"; do
    docker tag "$CI_REGISTRY_IMAGE:${CI_COMMIT_SHA}" "$CI_REGISTRY_IMAGE:${tag}"
    docker push "$CI_REGISTRY_IMAGE:${tag}"
  done
}

function is_latest_git_tag() {
  latest_tag=$(git tag -l '*.*' --sort=-v:refname | head -1)
  if ! git describe --tags --exact-match --match $latest_tag
  then
    return 1
  fi
}

function verify_tag_version() {
  repo_version_string=$(cat VERSION)
  if [ "v${repo_version_string}" != $CI_COMMIT_REF_NAME ]; then
    echo "Tagged commit $CI_COMMIT_REF_NAME did not match the value in the VERSION file: $repo_version_string" >&2
    exit 1
  fi
}
